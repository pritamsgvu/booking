import { Injectable } from '@angular/core';
import { of } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class DataService {

  sortByData = [
    { id: 'p-l-to-h', name: 'Price (Lowest to Highest)' },
    { id: 'p-h-to-l', name: 'Price (Highest to Lowest)' },
    { id: 'd-s-to-l', name: 'Duration (Shortest to Longest)' },
    { id: 'd-l-to-s', name: 'Duration (Longest to Shortest)' },
    { id: 'dp-e-to-l', name: 'Departure (Earliest to Latest)' },
    { id: 'ar-e-to-l', name: 'Arrival (Latest to Earliest)' },
    { id: 'a-a-to-z', name: 'Airline (A to Z)' },
    { id: 'a-z-to-a', name: 'Airline (Z to A)' },
  ]

  flightData = [
    // blr to bom 26/12 GoAir returning 27/12
    {
      "DepartureAirportCode": "BLR",
      "DepartureDateTime": "2021-12-26T05:45:00",
      "DestinationAirportCode": "BOM",
      "DestinationDateTime": "2021-12-26T07:10:00",
      "FlightNumber": 1,
      "StopQuantity": 0,
      "airLineName": "GoAir",
      "imageFileName": "http://live.arzoo.com/FlightWS/image/goair.gif",
      "BookingClass": {
        "Availability": 10,
      },
      "BookingClassFare": [
        {
          "adultFare": 10000,
          "classType": "Basic",
          "Availability": 3
        },
        {
          "adultFare": 12000,
          "classType": "Cabin",
          "Availability": 4
        },
        {
          "adultFare": 15000,
          "classType": "Economy",
          "Availability": 6
        }
      ],
      "DepartureDate": "2021-12-26",
      "returnStatus": {
        "DepartureAirportCode": "BOM",
        "DepartureDateTime": "2021-12-27T05:45:00",
        "DestinationAirportCode": "BLR",
        "DestinationDateTime": "2021-12-27T07:10:00",
        "FlightNumber": 1,
        "StopQuantity": 0,
        "airLineName": "GoAir",
        "imageFileName": "http://live.arzoo.com/FlightWS/image/goair.gif",
        "BookingClass": {
          "Availability": 10,
        },
        "BookingClassFare": [
          {
            "adultFare": 10000,
            "classType": "Basic",
            "Availability": 3
          },
          {
            "adultFare": 12000,
            "classType": "Cabin",
            "Availability": 4,
          },
          {
            "adultFare": 15000,
            "classType": "Economy",
            "Availability": 2,
          }
        ],
        "DepartureDate": "2021-12-27",
      }
    },
    // blr to bom 26/12 SpiceGet returning 27/12
    {
      "DepartureAirportCode": "BLR",
      "DepartureDateTime": "2021-12-26T07:50:00",
      "DestinationAirportCode": "BOM",
      "DestinationDateTime": "2021-12-26T10:00:00",
      "FlightNumber": 3,
      "StopQuantity": 1,
      "airLineName": "SpiceGet",
      "imageFileName": "http://live.arzoo.com/FlightWS/image/goair.gif",
      "BookingClass": {
        "Availability": 5,
      },
      "BookingClassFare": [
        {
          "adultFare": 9000,
          "classType": "Basic",
        },
        {
          "adultFare": 14000,
          "classType": "Cabin",
        },
        {
          "adultFare": 17000,
          "classType": "Economy",
        }
      ],
      "DepartureDate": "2021-12-26",
      "returnStatus": {
        "DepartureAirportCode": "BOM",
        "DepartureDateTime": "2021-12-27T07:50:00",
        "DestinationAirportCode": "BLR",
        "DestinationDateTime": "2021-12-27T10:00:00",
        "FlightNumber": 3,
        "StopQuantity": 1,
        "airLineName": "SpiceGet",
        "imageFileName": "http://live.arzoo.com/FlightWS/image/goair.gif",
        "BookingClass": {
          "Availability": 4,
        },
        "BookingClassFare": [
          {
            "adultFare": 9000,
            "classType": "Basic",
            "Availability": 2
          },
          {
            "adultFare": 14000,
            "classType": "Cabin",
            "Availability": 3
          },
          {
            "adultFare": 17000,
            "classType": "Economy",
            "Availability": 4
          }
        ],
        "DepartureDate": "2021-12-27",
      }
    },
    // blr to bom 26/12 Indigo returning 27/12
    {
      "DepartureAirportCode": "BLR",
      "DepartureDateTime": "2021-12-26T08:50:00",
      "DestinationAirportCode": "BOM",
      "DestinationDateTime": "2021-12-26T12:00:00",
      "FlightNumber": 2,
      "StopQuantity": 1,
      "airLineName": "Indigo",
      "imageFileName": "http://live.arzoo.com/FlightWS/image/goair.gif",
      "BookingClass": {
        "Availability": 9,
      },
      "BookingClassFare": [
        {
          "adultFare": 11000,
          "classType": "Basic",
          "Availability": 4
        },
        {
          "adultFare": 14000,
          "classType": "Cabin",
          "Availability": 1
        },
        {
          "adultFare": 16000,
          "classType": "Economy",
          "Availability": 6
        }
      ],
      "DepartureDate": "2021-12-26",
      "returnStatus": {
        "DepartureAirportCode": "BOM",
        "DepartureDateTime": "2021-12-27T08:50:00",
        "DestinationAirportCode": "BLR",
        "DestinationDateTime": "2021-12-27T12:00:00",
        "FlightNumber": 2,
        "StopQuantity": 1,
        "airLineName": "Indigo",
        "imageFileName": "http://live.arzoo.com/FlightWS/image/goair.gif",
        "BookingClass": {
          "Availability": 3,
        },
        "BookingClassFare": [
          {
            "adultFare": 11000,
            "classType": "Basic",
            "Availability": 1
          },
          {
            "adultFare": 14000,
            "classType": "Cabin",
            "Availability": 6
          },
          {
            "adultFare": 16000,
            "classType": "Economy",
            "Availability": 5
          }
        ],
        "DepartureDate": "2021-12-27"
      }
    },
    // bom to blr 26/12 Indigo returning 27/12
    {
      "DepartureAirportCode": "BOM",
      "DepartureDateTime": "2021-12-26T07:50:00",
      "DestinationAirportCode": "BLR",
      "DestinationDateTime": "2021-12-26T11:00:00",
      "FlightNumber": 2,
      "StopQuantity": 1,
      "airLineName": "Indigo",
      "imageFileName": "http://live.arzoo.com/FlightWS/image/goair.gif",
      "BookingClass": {
        "Availability": 9,
      },
      "BookingClassFare": [
        {
          "adultFare": 11000,
          "classType": "Basic",
          "Availability": 7
        },
        {
          "adultFare": 14000,
          "classType": "Cabin",
          "Availability": 4
        },
        {
          "adultFare": 16000,
          "classType": "Economy",
          "Availability": 3
        }
      ],
      "DepartureDate": "2021-12-26",
      "returnStatus": {
        "DepartureAirportCode": "BLR",
        "DepartureDateTime": "2021-12-27T09:50:00",
        "DestinationAirportCode": "BOM",
        "DestinationDateTime": "2021-12-27T12:00:00",
        "FlightNumber": 2,
        "StopQuantity": 1,
        "airLineName": "Indigo",
        "imageFileName": "http://live.arzoo.com/FlightWS/image/goair.gif",
        "BookingClass": {
          "Availability": 2,
        },
        "BookingClassFare": [
          {
            "adultFare": 11000,
            "classType": "Basic",
          },
          {
            "adultFare": 15000,
            "classType": "Cabin",
          },
          {
            "adultFare": 17000,
            "classType": "Economy",
          }
        ],
        "DepartureDate": "2021-12-27",
      }
    },
    // bom to blr 30/12 Indigo returning 31/12
    {
      "DepartureAirportCode": "BOM",
      "DepartureDateTime": "2021-12-30T08:50:00",
      "DestinationAirportCode": "BLR",
      "DestinationDateTime": "2021-12-30T12:00:00",
      "FlightNumber": 2,
      "StopQuantity": 1,
      "airLineName": "Indigo",
      "imageFileName": "http://live.arzoo.com/FlightWS/image/goair.gif",
      "BookingClass": {
        "Availability": 10,
      },
      "BookingClassFare": [
        {
          "adultFare": 11000,
          "classType": "Basic",
        },
        {
          "adultFare": 14000,
          "classType": "Cabin",
        },
        {
          "adultFare": 16000,
          "classType": "Economy",
        }
      ],
      "DepartureDate": "2021-12-30",
      "returnStatus": {
        "DepartureAirportCode": "BLR",
        "DepartureDateTime": "2021-12-31T08:50:00",
        "DestinationAirportCode": "BOM",
        "DestinationDateTime": "2021-12-31T12:00:00",
        "FlightNumber": 2,
        "StopQuantity": 1,
        "airLineName": "Indigo",
        "imageFileName": "http://live.arzoo.com/FlightWS/image/goair.gif",
        "BookingClass": {
          "Availability": 9,
        },
        "BookingClassFare": [
          {
            "adultFare": 11000,
            "classType": "Basic",
          },
          {
            "adultFare": 14000,
            "classType": "Cabin",
          },
          {
            "adultFare": 16000,
            "classType": "Economy",
          }
        ],
        "DepartureDate": "2021-12-31",
      }
    }
  ]

  city = [
    {
      code: 'BOM',
      city: 'Bombay'
    },
    {
      code: 'DEL',
      city: 'Delhi'
    },
    {
      code: 'PUN',
      city: 'Pune'
    },
    {
      code: 'PAT',
      city: 'Patna'
    }

  ]

  bookingClass = [
    {
      id: 1,
      classType: "Basic",
    },
    {
      id: 2,
      classType: "Cabin",
    },
    {
      id: 3,
      classType: "Economy"
    }
  ]

  airline = [
    {
      id: 1,
      name: "GoAir",
      isChecked: true
    },
    {
      id: 2,
      name: "Indigo",
      isChecked: true
    },
    {
      id: 3,
      name: "SpiceGet",
      isChecked: true

    },
  ]

  constructor() { }

  getFlightData(param) {
    if (param) {
      param.departure = param.departure.toUpperCase();
      param.destination = param.destination.toUpperCase();
      param.departureDate = this.getFormattedDate(param.departureDate);
      param.returnDate = param.returnDate ? this.getFormattedDate(param.returnDate) : '';
      let newArray = this.flightData.filter((data) => {
        let newVal;
        if (param.tripTypeField == '0') {
          newVal = ((param.departure == data.DepartureAirportCode) || (param.departure == data.returnStatus?.DepartureAirportCode
            && param.departureDate == data.returnStatus?.DepartureDate))
            && ((param.destination == data.DestinationAirportCode) || (param.destination == data.returnStatus?.DestinationAirportCode))
            && ((param.departureDate == data.DepartureDate) || (param.departureDate == data.returnStatus?.DepartureDate))
        } else {
          newVal = (param.departure == data.DepartureAirportCode) &&
            (param.destination == data.DestinationAirportCode) &&
            (param.departureDate == data.DepartureDate) &&
            (param.returnDate == data.returnStatus?.DepartureDate)
        };
        return newVal

      });
      localStorage.setItem('flightData', JSON.stringify(newArray))
      //return of(newArray);
    }
  }

  getCity() {
    return of(this.city);
  }

  getDifferenceInSeconds(date1, date2) {
    const diffInMs = Math.abs(date2 - date1);
    return diffInMs / 1000;
  }

  convertInToSecond(date) {
    date = new Date(date);
    return date.getTime() / 1000;
  }

  getFormattedDate(date) {
    const month = new Date(date).getMonth() + 1;
    return (new Date(date).getFullYear() + '-' + month + '-' + new Date(date).getDate());
  }

  sortData(param, flightData) {
    let newArray = [];
    if (param) {
      switch (param) {
        case 'p-l-to-h':
          newArray = flightData.sort((a, b) => {
            return a.BookingClassFare[0].adultFare - b.BookingClassFare[0].adultFare;
          });
          break;
        case 'p-h-to-l':
          newArray = flightData.sort((a, b) => {
            return b.BookingClassFare[0].adultFare - a.BookingClassFare[0].adultFare;
          });
          break;
        case 'd-s-to-l':
          newArray = flightData.sort((a, b) => {
            const vart = this.getDifferenceInSeconds(new Date(a.DestinationDateTime), new Date(a.DepartureDateTime)) - this.getDifferenceInSeconds(new Date(b.DestinationDateTime), new Date(b.DepartureDateTime));
            return vart;
          });
          break;
        case 'd-l-to-s':
          newArray = flightData.sort((a, b) => {
            const vart = this.getDifferenceInSeconds(new Date(b.DestinationDateTime), new Date(b.DepartureDateTime)) - this.getDifferenceInSeconds(new Date(a.DestinationDateTime), new Date(a.DepartureDateTime));
            return vart;
          });
          break;
        case 'dp-e-to-l':
          newArray = flightData.sort((a, b) => {
            const vart = this.convertInToSecond(a.DepartureDateTime) - this.convertInToSecond(b.DepartureDateTime);
            return vart;
          });
          break;
        case 'ar-e-to-l':
          newArray = flightData.sort((a, b) => {
            const vart = this.convertInToSecond(b.DestinationDateTime) - this.convertInToSecond(a.DestinationDateTime);
            return vart;
          });
          break;
        case 'a-a-to-z':
          newArray = flightData.sort(function (a, b) {
            let nameA = a.airLineName.toUpperCase();
            let nameB = b.airLineName.toUpperCase();
            if (nameA < nameB) {
              return -1;
            }
            if (nameA > nameB) {
              return 1;
            }
            return 0;
          });
          break;
        case 'a-z-to-a':
          newArray = flightData.sort(function (a, b) {
            let nameA = a.airLineName.toUpperCase();
            let nameB = b.airLineName.toUpperCase();
            if (nameA > nameB) {
              return -1;
            }
            if (nameA < nameB) {
              return 1;
            }
            return 0;
          });
          break;
        default:
          newArray = flightData;
      }
      return of(newArray);
      // localStorage.setItem('flightData', JSON.stringify(newArray))

    }
  }

  filterDataFromPage(param, flightData) {
    if (param) {
      let newArray = flightData.filter((data) => {
        return data.BookingClassFare[0].adultFare >= param.minimum
          && data.BookingClassFare[0].adultFare <= param.maximum &&
          param.airline.some((f) => {
            if (f) {
              return data.airLineName == f;
            } else {
              return true;
            }
          });
      });
      return of(newArray);
    }
  }
}

