import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';
import { ActivatedRoute, NavigationStart, Router } from '@angular/router';
import { DataService } from 'src/app/data.service';
import { Event as NavigationEvent } from "@angular/router";
import { filter } from "rxjs/operators";

@Component({
  selector: 'app-details',
  templateUrl: './details.page.html',
  styleUrls: ['./details.page.scss'],
})
export class DetailsPage implements OnInit {

  flightData = [];
  queryParams: any;
  loading = true;
  constructor(private nav: NavController, private router: Router, private dataSvc: DataService, private route: ActivatedRoute) {
    this.router.events
      .pipe(
        filter(
          (event: NavigationEvent) => {
            return (event instanceof NavigationStart);
          }
        )
      )
      .subscribe(
        (event: NavigationStart) => {
          if (event.url === '/details') {
            this.flightData = JSON.parse(localStorage.getItem('flightData'));
          }
        }
      );
  }


  ngOnInit() {

    setTimeout(() => {
      this.loading = false;
    }, 1000)

    this.flightData = JSON.parse(localStorage.getItem('flightData'));

    this.route.queryParams.pipe()
      .subscribe(params => {
        if (Object.entries(params).length > 0) {
          // for filter
          if (Object.entries(params).length > 0 && (!params.selectedValue || params.selectedValue == undefined)) {
            this.dataSvc.filterDataFromPage(params, JSON.parse(localStorage.getItem('flightData'))).subscribe((data: any) => {
              this.flightData = data;
            })
          }
          // for sorting
          if (params.selectedValue && Object.entries(params.selectedValue).length > 0) {
            this.dataSvc.sortData(params.selectedValue, this.flightData).subscribe((data: any) => {
              this.flightData = data;
            })
          }
        }
      });
    this.queryParams = JSON.parse(localStorage.getItem('filterRequest'));
  }

  openPage(page: any) {
    this.nav.navigateForward(page);
  }

  getDataDiff(startDate, endDate) {
    startDate = new Date(startDate)
    endDate = new Date(endDate)
    var diff = endDate.getTime() - startDate.getTime();
    var days = Math.floor(diff / (60 * 60 * 24 * 1000));
    var hours = Math.floor(diff / (60 * 60 * 1000)) - (days * 24);
    var minutes = Math.floor(diff / (60 * 1000)) - ((days * 24 * 60) + (hours * 60));
    return hours + 'h ' + minutes + 'm';
  }
}
