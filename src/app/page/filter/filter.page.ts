import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { DataService } from 'src/app/data.service';

@Component({
  selector: 'app-filter',
  templateUrl: './filter.page.html',
  styleUrls: ['./filter.page.scss'],
})
export class FilterPage implements OnInit {

  public bookingClass = this.dataSvc.bookingClass;

  public airlineData = this.dataSvc.airline;

  filterData: any;
  filterForm: FormGroup;
  airlineArr = ['GoAir', 'Indigo', 'SpiceGet'];
  selectedArray: any = [];

  constructor(private formBuilder: FormBuilder,
    private router: Router, private dataSvc: DataService) { }

  stepValue = { lower: '2000', upper: '20000' };

  ngOnInit() {
    this.formInit();
    this.dataMapping();
  }

  formInit() {
    this.filterData = localStorage.getItem('filterData') && localStorage.getItem('filterData').length > 0 ? JSON.parse(localStorage.getItem('filterData')) : [];
    this.filterForm = this.formBuilder.group({
      minimum: [this.filterData?.minimum ? this.filterData?.minimum : 2000],
      maximum: [this.filterData?.maximum ? this.filterData?.maximum : 20000],
      airline: this.formBuilder.array(this.filterData?.airline?.length > 0 ? this.filterData?.airline : this.airlineArr)
    });
    this.stepValue = { lower: this.filterData?.minimum ? this.filterData?.minimum : '2000', upper: this.filterData?.maximum ? this.filterData?.maximum : '20000' };
    this.filterForm.controls['minimum'].valueChanges.subscribe(change => {
      this.stepValue.lower = change;
    });
    this.filterForm.controls['maximum'].valueChanges.subscribe(change => {
      this.stepValue.upper = change;
    });
  }

  dataMapping() {
    this.airlineData.map((e) => {
      let airlineVal = this.filterForm.controls['airline'].value;
      if (airlineVal.length > 0 && airlineVal.indexOf(e.name) !== -1) {
        e.isChecked = true;
      } else {
        e.isChecked = false;
      }
    })
  }

  onSubmit() {
    localStorage.setItem('filterData', JSON.stringify(this.filterForm.value));
    this.router.navigate(['details'], { queryParams: this.filterForm.value });
  }

  onReset() {
    this.filterForm.patchValue({ minimum: 2000, maximum: 20000 })
    this.filterForm.setControl('airline', this.formBuilder.array(this.airlineArr));
    this.dataMapping();
    localStorage.setItem('filterData', JSON.stringify(this.filterForm.value));
  }

  getStepValue(e) {
    const steps = e.target.value;
    this.filterForm.patchValue({ minimum: steps.lower, maximum: steps.upper });
  }

  onChange(airline: string, isChecked: boolean) {
    const airlineFormArray = this.filterForm.controls['airline'].value;
    if (!isChecked) {
      airlineFormArray.push(airline);
    } else {
      let index = airlineFormArray.indexOf(airline)
      airlineFormArray.splice(index, 1);
    }
    this.filterForm.setControl('airline', this.formBuilder.array(airlineFormArray));
  }

}
