import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { SortPageRoutingModule } from './sort-routing.module';

import { SortPage } from './sort.page';
import { DataService } from 'src/app/data.service';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SortPageRoutingModule
  ],
  providers: [DataService],
  declarations: [SortPage]
})
export class SortPageModule { }
