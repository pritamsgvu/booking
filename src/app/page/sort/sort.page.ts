import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { DataService } from 'src/app/data.service';

@Component({
  selector: 'app-sort',
  templateUrl: './sort.page.html',
  styleUrls: ['./sort.page.scss'],
})
export class SortPage implements OnInit {

  sortByData: any;
  constructor(private dataSvc: DataService, private router: Router) { }

  selectedValue: any = '';

  ngOnInit() {
    this.sortByData = this.dataSvc.sortByData;
    this.selectedValue = localStorage.getItem('selectedValue');
  }

  selectedVal(val: any) {
    this.selectedValue = val.target.value;
  }

  submit() {
    localStorage.setItem('selectedValue', this.selectedValue)
    this.router.navigate(['details'], { queryParams: { selectedValue: this.selectedValue } });
  }

}
