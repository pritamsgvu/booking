import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { DataService } from 'src/app/data.service';
@Component({
  selector: 'app-home',
  templateUrl: './home.page.html',
  styleUrls: ['./home.page.scss'],
})
export class HomePage implements OnInit {
  flightSearchForm: FormGroup;
  submitted = false;
  isDisabled = true;
  cityData: [];
  tripType = [
    { id: 1, name: 'One Way' },
    { id: 2, name: 'Round Trip' }
  ]
  data: Array<any> = [
    { id: 1, title: 'Flights', icon: 'airplane' },
    { id: 2, title: 'Hotels', icon: 'business' },
    { id: 2, title: 'Cars', icon: 'car' },
    { id: 2, title: 'Add', icon: 'flag' },
  ];
  constructor(private router: Router, private dataSvc: DataService,
    private formBuilder: FormBuilder) {
  }

  ngOnInit() {
    this.flightSearchForm = this.formBuilder.group({
      tripTypeField: ['0', Validators.required],
      departure: ['BLR', Validators.required],
      destination: ['BOM', Validators.required],
      departureDate: ['2021-12-26', [Validators.required]],
      returnDate: [{ value: null, disabled: this.isDisabled }, [Validators.required]],
      travelers: ['2', [Validators.required]],
      class: ['cabin', [Validators.required]],
    });

    this.flightSearchForm.controls['tripTypeField'].valueChanges.subscribe(change => {
      if (change == 0) {
        this.flightSearchForm.patchValue({ returnDate: '' });
        this.flightSearchForm.controls['returnDate'].disable();
      } else {
        this.flightSearchForm.controls['returnDate'].enable();
        this.flightSearchForm.patchValue({ returnDate: '2021-12-27' });

      }
    });
  }

  onSubmit() {
    this.submitted = true;
    // invalid check
    if (this.flightSearchForm.invalid) {
      return;
    } else {
      localStorage.setItem('filterRequest', JSON.stringify(this.flightSearchForm.value));
      this.dataSvc.getFlightData(this.flightSearchForm.value);
      this.router.navigate(['details'], this.flightSearchForm.value);
    }
  }

  onReset() {
    this.submitted = false;
    this.flightSearchForm.reset();
  }

}
