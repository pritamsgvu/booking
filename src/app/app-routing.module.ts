import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'home',
    pathMatch: 'full'
  },
  {
    path: 'home',
    loadChildren: () => import('./page/home/home.module').then(m => m.HomePageModule)
  },
  {
    path: 'details',
    loadChildren: () => import('./page/details/details.module').then(m => m.DetailsPageModule)
  },
  {
    path: 'filter',
    loadChildren: () => import('./page/filter/filter.module').then( m => m.FilterPageModule)
  },
  {
    path: 'sort',
    loadChildren: () => import('./page/sort/sort.module').then( m => m.SortPageModule)
  }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
