import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { MenuController } from '@ionic/angular';
@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss'],
})
export class AppComponent implements OnInit {
  constructor(private router: Router, private menu: MenuController) { }

  ngOnInit() {
    this.router.navigate(['']);
    localStorage.setItem('flightData', '');
    localStorage.setItem('filterData', '');
    localStorage.setItem('filterRequest', '');
    localStorage.setItem('selectedValue', '');
  }

  openEnd() {
    this.menu.close();
  }
}
